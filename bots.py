from controller_interface import ControllerInterface
from random import randint
from math import pow, sqrt


class RandomBot(ControllerInterface):
    def take_action(self, weights: tuple) -> int:
        return randint(0, 5)

    def compute_features(self, sensors: tuple) -> tuple:
        return 1, 1

    def learn(self, weights: tuple):
        return


class ParasiteBot(ControllerInterface):
    def take_action(self, weights: tuple):
        x1, y1 = self.sensors['drill_position']
        x2, y2 = self.sensors['enemy_1_drill_mothership_position']

        if self.sensors['drill_gas'] < 50:
            align = self.sensors['align_mothership']
        else:
            align = self.sensors['align_enemy_mothership']

        distance = sqrt(pow(x1-x2, 2) + pow(y1-y2, 2))

        if align:
            return self.ACCELERATE
        elif self.sensors['drill_touching_mothership'] == 1 and distance < 300:
            return self.NOTHING
        else:
            return self.LEFT

    def compute_features(self, sensors: tuple) -> tuple:
        return 1, 1

    def learn(self, weights: tuple):
        return


class CollectorBot(ControllerInterface):

    def take_action(self, weights: tuple):

        if self.sensors['drill_gas'] < 50:
            align = self.sensors['align_mothership']
        else:
            align = self.sensors['align_asteroid']

        if align:
            return self.ACCELERATE
#        elif self.sensors['drill_touching_mothership']:
#            return self.NOTHING
        else:
            return self.LEFT

    def compute_features(self, sensors: tuple) -> tuple:
        return 1, 1

    def learn(self, weights: tuple):
        return


class CustomBot(ControllerInterface):
    pass

