from controller_interface import ControllerInterface
import random
import math
import numpy as np
import time
import matplotlib.pyplot as plt 

previous_distance = 2000
previous_distance_to_enemy_mother = 2000
num_features = 0
class Controller(ControllerInterface):
    
    def take_action(self, weights: tuple) -> int:
        """
        :return: An integer corresponding to an action:
        1 - Right
        2 - Left
        3 - Accelerate forward
        4 - Discharge
        5 - Nothing
        """
        #print(str(weights))
        #time.sleep(0.1)
        actions = ('R', 'L', 'A', 'D', 'N')
        action_values = [0, 0, 0, 0, 0]
        
        features = self.compute_features(self.sensors)
        
        for z in range(5):                  #for each action
            initial_weight_index = z + len(features)*z
            action_values[z] = action_values[z] + weights[initial_weight_index]
            #print("Valor da Ação " + str(z) + ":  " + str(action_values[z]))
            for i in range(len(features)):      #for each feature
                action_values[z] = action_values[z] + weights[initial_weight_index + 1 + i] * features[i]
        
        #for i in range(5):
            #print("Valor da Ação " + str(i) + ":  " + str(action_values[i]))
            
        # print("Ação escolhida: " + str(np.argmax(action_values)))
        # print("----------------------------------------------")
        #time.sleep(0.05)
        
        #for idx,feature in enumerate(features):
            #for i in range(len(actions)):
                #action_values[i] += weights[len(features)*i + idx] * feature
                
        return np.argmax(action_values)+1

    def normalize_feature(self, feature_value, min, max):
        return (2 * (feature_value-min)/(max-min) - 1)

    # def distance(sensor1, sensor2):
        # math.sqrt( (sensors[sensor1][0] - sensors[sensor2][0])**2 + (sensors[sensor1][1] - sensors[[sensor2][1])**2)

    def compute_features(self, sensors: dict) -> tuple:
        features = ()
        
        global previous_distance
        global previous_distance_enemy_to_mother

        #Feature: Distance between the drill and the asteroid
        distance_to_asteroid = math.sqrt( (sensors['drill_position'][0] - sensors['asteroid_position'][0])**2 + (sensors['drill_position'][1] - sensors['asteroid_position'][1])**2)
        diff_distance = previous_distance - distance_to_asteroid
        norm_diff_distance = self.normalize_feature(diff_distance * sensors['asteroid_resources'], 0, 2000) # distance between (0,0) and (1000,700)
        features = features + (norm_diff_distance,) # distance to the asteroid
        previous_distance = distance_to_asteroid
        #-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        #Feature: Angle between the drill and the asteroid
        norm_ship_velocity = math.sqrt(sensors['drill_velocity'][0]**2 + sensors['drill_velocity'][1]**2)
        norm_asteroid_velocity = math.sqrt(sensors['asteroid_velocity'][0]**2 + sensors['asteroid_velocity'][1]**2)
        dot_ship_asteroid = np.dot(norm_asteroid_velocity, norm_ship_velocity)
        angle_ship_asteroid = np.arccos(dot_ship_asteroid / ((norm_asteroid_velocity * norm_ship_velocity) + 0.1))
        angle_ship_asteroid = self.normalize_feature(angle_ship_asteroid, 0, 360) # angle between ship and asteroid
        features = features + (angle_ship_asteroid,)
        #------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        
        #Feature: Relation of distance to mothership and available gas
        distance_to_mother = math.sqrt( (sensors['drill_position'][0] - sensors['drill_mothership_position'][0])**2 + (sensors['drill_position'][1] - sensors['drill_mothership_position'][1])**2)
        dist_by_gas = self.normalize_feature((sensors['drill_gas'])/(distance_to_mother+1), 0, 200)
        features = features + (dist_by_gas,) # distance to the mothership related to the available fuel
        #------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        features = features + (sensors['align_asteroid'],)
        features = features + (sensors['align_mothership'],)
        features = features + (sensors['align_enemy_mothership'],)
        #Feature: 
        
        #------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        #features = features + (self.normalize_feature(sensors['drill_discharge_cooldown'], 0, 100),)
    
        #Feature: 
        
        #------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        #Feature: Current cooldown for the taser
        taser_cooldown = self.normalize_feature(sensors['drill_discharge_cooldown'], 0, 100)
        features = features + (taser_cooldown,)
        #------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        #Feature: Relation using the enemy's distance to his mothership, the enemy's available gas and the drill's taser cooldown
        #enemy_to_mothership = math.sqrt( (sensors['enemy_1_drill_position'][0] - sensors['enemy_1_drill_mothership_position'][0])**2 + (sensors['enemy_1_drill_position'][1] - sensors['enemy_1_drill_mothership_position'][1])**2)
        #enemy_gas_mothership_ratio = (enemy_to_mothership/(sensors['enemy_1_drill_gas']+1))
        #enemy_far_and_taser = enemy_gas_mothership_ratio * taser_cooldown
        #enemy_gas_mothership_ratio = self.normalize_feature(enemy_far_and_taser, 0, 200000)
        #features = features + (enemy_far_and_taser,)

        features = features + (self.normalize_feature(sensors['drill_gas'],0,200),)
               
        return features

        """
        This function should take the raw sensor information of the ship (see below) and compute useful features
        for selecting an action.
        The ship has the following sensors:

        :param sensors: contains:
             'asteroid_position': (x, y)
             'asteroid_velocity': (n, m)
             'asteroid_resources': 0 - ???
             'align_asteroid': 0 or 1
             'align_mothership': 0 or 1
             'drill_angle': angle in rad
             'drill_position': (x, y)
             'drill_velocity': (n, m)
             'drill_mothership_position': (x, y)
             'drill_resources': 0 - ???
             'drill_touching_asteroid': 0 or 1
             'drill_touching_mothership': 0 or 1
             'drill_discharge_cooldown': 0 - COOLDOWN (defined in config.py)
             'drill_edge_position': (x, y)
             'drill_gas': 0 - MAX_GAS (defined in config.py)
             'enemy_1_drill_angle': angle in rad
             'enemy_1_drill_position': (x, y)
             'enemy_1_drill_velocity': (n, m)
             'enemy_1_drill_mothership_position': (x, y)
             'enemy_1_drill_resources': 0 - 0
             'enemy_1_drill_touching_asteroid': 0 or 1
             'enemy_1_drill_touching_mothership': 0 or 1
             'enemy_1_drill_discharge_cooldown': 0 - COOLDOWN  (defined in config.py)
             'enemy_1_drill_edge_position': (x, y)
             'enemy_1_drill_gas': 0 - MAX_GAS (defined in config.py)
        :return: A Tuple containing the features you defined
        """

    def individual_to_file(self, individual, file_name):
        file_object = open(file_name, 'w')
        
        file_object.write(str(individual[0]))
        for i in range(1,len(individual)):
            file_object.write(" " + str(individual[i]))
        
        file_object.close()
        
    def generation_to_file(self, best_score, num_gen, file_name):
        file_object = open(file_name, 'a')
        
        file_object.write(str(num_gen) + "," + str(best_score) + " ")
        
        file_object.close()
            
    def generate_initial_pop(self, weights: tuple, population_size):
        population = []
        population.append(weights)        
        
        for i in range(1, population_size):                                 #create a list of weights with population_size individuals
            new_weights = []
            for j in range(len(weights)):
                random_number = random.uniform(-3, 3)      #each individual consists of a small variation of the original weights tuple given as parameter
                new_weights = new_weights + [random_number]
            population.append(tuple(new_weights))

        # print(population[0])

        return population

    def evaluate_pop(self,population):
        scores = []
        for i in range(len(population)):    
            temp_score =  self.run_episode(population[i])   #calculate scores
            if temp_score == 0:
                temp_score = 0.00000001
            scores.append(temp_score)

        print("max score of population = " + str(max(scores)))

        # for i in range(len(scores)):            #normalize the scores based on the maximum score obtained
            # scores[i] = scores[i] / (max(scores))
        return scores

    def select_individuals(self,population, scores, population_size, tournament_size):
        selected = []
        selected.append(population[scores.index(max(scores))])

        for i in range(1,population_size):
            k_individuals = random.sample(range(len(population)), tournament_size)
            winner_score = scores[k_individuals[0]]
            for j in k_individuals:
                if scores[j] >= winner_score:
                    winner_score = scores[j]
                    tournament_winner = j
            #print("Selecting " + str(tournament_winner) + " with score " + str(scores[tournament_winner]))
            selected.append(population[tournament_winner])
        return selected

    def reproduce(self,selected, population_size, top_best):
        new_population = []
        for i in range(population_size - top_best):
            indexA = random.randint(0,population_size-1)
            indexB = random.randint(0,population_size-1)
            individualA = selected[indexA]
            individualB = selected[indexB]
            newIndividual = []
            for i in range(len(individualA)):
                mask = random.randint(0, 1)
                if mask == 0:
                    newIndividual.append(individualA[i])
                else:
                    newIndividual.append(individualB[i])
                    
            newIndividual= tuple(newIndividual)

            random_number = random.uniform(1,10)
            newIndividual = self.mutate(newIndividual, random_number)

            new_population.append(newIndividual)
        return new_population

    def mutate(self, individual, option):
        mutated_individual = individual
        if option <= 5:
            return tuple(mutated_individual)
        elif option <= 8.5:
            return tuple(self.softMutation(mutated_individual))
        elif option <= 9.5:
            return tuple(self.hardMutation(mutated_individual))
        else:
            return tuple(self.scrambleMutation(mutated_individual))

    def softMutation(self, individual):
        mutation = 0.3
        mutated_individual = individual
        
        for weigth in mutated_individual:
            chance = random.randint(0, 1)
            if chance == 0:
                weigth = weigth + random.uniform(-mutation, mutation)
        
        return mutated_individual

    def hardMutation(self, individual):
        mutated_individual = list(individual)
        index = random.randint(0, len(individual)-1)
        mutated_individual[index] *= -1
        
        return tuple(mutated_individual)

    def scrambleMutation(self, individual):
        mutated_individual = []
        global num_features
        mutated_individual = list(individual)
        weights = []
        random_feature_index = 1 + random.randint(0,num_features)
        for i in range(5): #for each action
            weights.append(individual[random_feature_index + num_features * i])
            
        random.shuffle(weights)
        
        for i in range(5): #for each action
            mutated_individual[random_feature_index + num_features * i] = weights[i] 
        return tuple(mutated_individual)

    temp = 0
    
    def genetic_alg(self, weights: tuple):
        population_size = 50
        number_iterations = 30
        tournament_size = 7
        population = []
        scores = []
        selected_individuals = []
        best_individual = weights
        best_score = self.run_episode(weights)
        new_population = self.generate_initial_pop(weights, population_size)
        top_best = int(population_size*0.1)

        for i in range(number_iterations):
            population = new_population
            #print("NEW POPULATION: " + str(new_population))
            scores = self.evaluate_pop(population)
            selected_individuals = self.select_individuals(population, scores, population_size, tournament_size)
            new_population = self.reproduce(selected_individuals, population_size, top_best)
            top_best_index = np.argsort(scores)[-top_best:]
            print (top_best, top_best_index, scores)
            for j in range (0, top_best):
                new_population.append(population[top_best_index[j]])
            if max(scores) > best_score: 
                best_individual = population[scores.index(max(scores))]
                best_score = max(scores)
            self.generation_to_file(best_score, i+1, "scores_gen.txt")
            self.individual_to_file(population[scores.index(max(scores))], "temp_weights")

        return best_individual

    def simulated_annealing(self, weights : tuple, temperature, change):
        current_state = weights.copy()
        number_weights = len(weights)
        number_changes = int(len(weights)) #* 0.50) #changes 50% of the weights
        # change = change
        previous_score = 0
        current_score = self.run_episode(current_state)

        print("previous score = " + str(previous_score))
        print("current score = " + str(current_score))

        while True:
            if temperature == 0:
                if count_improv == 20:
                    return current_state
                else:
                    count_improv = count_improv + 1
                
            temperature = temperature - 0.5                                                 # until improvement lowers enough
            previous_score = current_score

            for w in range(len(weights)):
                neighbour = current_state.copy()
                
                neighbour[w] += change
                neighbour_score = self.run_episode(neighbour)
                delta_e = neighbour_score - current_score 
                
                if delta_e > 0 or random.uniform(0,1) < math.exp(delta_e/temperature):
                    current_state = neighbour
                    current_score = neighbour_score
                        
            print("previous_score = " + str(previous_score))
            print("new current score = " + str(current_score))
            self.generation_to_file(best_score, i+1, "scores_sa.txt")


        return current_state

    def generate_neighbour(self, current_state, indexes, change): ### ALGUEM GERA VIZINHOS MELHORES PLOX ###
        neighbour_state_minus = current_state.copy()
        neighbour_state_plus = current_state.copy()
        for i in indexes:
            neighbour_state_minus[i] = neighbour_state_minus[i] + neighbour_state_minus[i] * -change            
            neighbour_state_plus[i] = neighbour_state_plus[i] + neighbour_state_minus[i] * change
        return neighbour_state_minus, neighbour_state_plus

    def hill_climbing(self, weights : tuple, neighbours):
        current_state = weights
        number_weights = len(weights)
        number_neighbours = neighbours
        number_changes = int(len(weights) )# * 0.50) #changes 50% of the weights
        change = 0.05    # each weights will be perturbed by + or - 5% of its value
        previous_score = 0
        current_score = self.run_episode(current_state)
        count_improv = 0

        while count_improv <= 5:
            if current_score - previous_score < 1:
                count_improv = count_improv + 1 
            
            previous_score = current_score 
            neighbours = [current_state]                                                                 # resets neighbours

            for i in range(number_neighbours):                                              
                perturbed_indexes = random.sample(range(number_weights), number_changes)    # selects M indexes to be perturbed
                new_neighbour_minus, new_neighbour_plus = self.generate_neighbour(current_state, perturbed_indexes, change) # changes neighbours on M indexes
                neighbours.append(new_neighbour_minus)
                neighbours.append(new_neighbour_plus)
                
            scores = list(map(lambda x : self.run_episode(x), neighbours)) 
            new_score = max(scores)

            if new_score >= current_score:
                current_state = neighbours[scores.index(new_score)]                           # updates current state to best neighbour 
                current_score = new_score

            print("previous_score = " + str(previous_score))
            print("new score = " + str(current_score))
            # print("new weights = " + str(tuple(current_state)))
 
        return current_state

    def learn(self, weights: tuple):
        #print(str(weights) + "quando entraram")
        #best_weights = self.genetic_alg(weights)
        #best_weights = self.hill_climbing(weights, 40)
        best_weights = self.simulated_annealing(weights, 1000, 0.025)
        self.individual_to_file(best_weights, "weights.txt")
        print(tuple(best_weights))

